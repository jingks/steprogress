﻿(function()
{
	var Steprogress = function(param)
	{
		return new Steprogress.prototype.init(param);
	}

	Steprogress.prototype = {
		init: function(param){
			var stp     = {};
			stp.ele     = param.ele      || '#steprogress';
			stp.color   = param.color    || ['#ef9090', '#ef2424'];
			stp.title   = param.title    || [1, 2, 3, 4, 5];
			stp.content = param.content  || ['one', 'two', 'three', 'four', 'five'];
			stp.total   = param.total    || 5;
			stp.min     = param.min      || 0;
			stp.max     = param.max      || 100;
			stp.current = param.current  || 60;
			this.stp    = stp;
			return this;
		},
		show: function()
		{
			//渲染底部bar
			var stp = this.stp;
			var ele = document.querySelector(stp.ele);
			var bar1 = document.createElement("div");
			bar1.className = "progress progress1";
			bar1.style.background = stp.color[0];
			ele.appendChild(bar1);

			//渲染二层bar
			var percent = stp.current / (stp.max - stp.min);
			var bar2 = document.createElement("div");
			bar2.className = "progress progress2";
			bar2.style.background = stp.color[1];
			bar2.style.width = percent * 100 + '%';
			ele.appendChild(bar2);

			//渲染point以及文字
			point = [];
			for(var i = 0; i < stp.total; i++)
			{
				point[i] = document.createElement("div");
				point[i].className = "point";

				//node1
				var node1 = document.createElement("span");
				node1.className = "node1";
				node1.style.background = stp.color[0];
				node1Text = stp.title[i] != undefined ? stp.title[i] + '件' : 'title';
				var text1 = document.createTextNode(node1Text);
				node1.appendChild(text1);
				point[i].appendChild(node1);

				//node2
				var node2 = document.createElement("span");
				node2.className = "node2";
				node2.style.background = stp.color[0];
				node2Text = stp.title[i] != undefined ? '￥' + stp.content[i] : 'content';
				var text2 = document.createTextNode(node2Text);
				node2.appendChild(text2);
				point[i].appendChild(node2);

				if(i == 0)
				{
					point[i].style.background = stp.color[1];
				}
				else
				{
					point[i].style.left = stp.title[i] / (stp.max - stp.min) * 100 + '%';
					if(i / (stp.total - 1) > percent)
					{
						point[i].style.background = stp.color[0];
					}
					else
					{
						point[i].style.background = stp.color[1];
					}
				}
				ele.appendChild(point[i]);
			}

			//渲染arrow及文字
			var arrow = document.createElement("div");
			arrow.className = "arrow";
			arrow.style.borderTopColor = stp.color[1];
			arrow.style.left = percent * 100 + '%';
			var node = document.createElement("span");
			var text = document.createTextNode(stp.current + '件');
			node.appendChild(text);
			arrow.appendChild(node);
			ele.appendChild(arrow);
		},
		animate: function()
		{
			//pass
		}
	}

	Steprogress.version = function()
	{
		alert('0.0.1');
	}

	Steprogress.prototype.init.prototype = Steprogress.prototype;

	window.Steprogress = Steprogress;

})(window)